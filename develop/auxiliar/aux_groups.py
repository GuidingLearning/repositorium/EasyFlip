import re
from sys import maxsize
import discord
import random
import parse
GROUP_NAME_PATTERN = re.compile("^A-")
from auxiliar.global_variables import PROFESSOR_ROLE_NAME, AUXILIAR_ROLE_NAME, ASSISTANT_ROLE_NAME

ROLES = [PROFESSOR_ROLE_NAME, AUXILIAR_ROLE_NAME, ASSISTANT_ROLE_NAME]

def existing_groups(guild):
    return [group for group in guild.categories if GROUP_NAME_PATTERN.search(group.name)]

def aux_list_existing_groups(groups):
    i = 1
    current_groups = list()
    for category in sorted(groups, key=lambda c: c.name):
        pattern = re.compile(f"^A-group-{i}")
        if re.search(pattern, category.name) is None:
            break
        current_groups.append(category)
        i+= 1
    return current_groups

async def aux_create_groups(guild, i, n):
    groups = list()
    while i <= n:
        new_category_name = f"A-group-{i}"
        text_channel_name = f"A-text-{i}"
        voice_channel_name = f"A-voice-{i}"
        print(f'Creating a new category: {new_category_name}')
        new_category = await guild.create_category_channel(new_category_name)
        print(f'Creating a new channels: ({text_channel_name}) and ({voice_channel_name})')
        await new_category.create_text_channel(text_channel_name)
        await new_category.create_voice_channel(voice_channel_name)
        groups.append(new_category)
        i+=1
    return groups


def aux_students_online(members):
    members_online = list()
    for member in members:
        if member.voice and  member.voice.channel and is_student(member):
            members_online.append(member)
    return members_online

def is_student(member):
    for role in member.roles:
        if role.name in ROLES:
            return False
    return True

async def aux_move_students(online_students, groups, maxsize):
    voice_channels = await aux_voice_channels(groups)
    for user in online_students:
        r = random.randint(0,len(voice_channels)-1)
        while(len(voice_channels[r].members) + 1>maxsize):
            r = random.randint(0,len(voice_channels)-1)
        await user.move_to(voice_channels[r])

async def aux_move_general(online_students, general):
    for user in online_students:
        await user.move_to(general)


async def aux_move_students_by_choices(online_students, groups, an):
    voice_channels = await aux_voice_channels(groups)
    w = "A"
    i = 0
    number_of_choices = an["question"]["number of choices"]
    final_word = chr(ord(w) + number_of_choices )
    while w < final_word:
        if an.get(w):
            for user in an[w]:
                if user in online_students:
                    await user.move_to(voice_channels[i])
        w=chr(ord(w) + 1)
        i+=1
    if an.get("sin respuesta") is not None:
        for user in an["sin respuesta"]:
            if user in online_students:
                    await user.move_to(voice_channels[i])


async def aux_dispersed_groups(online_students, groups, an, maxsize):
    voice_channels = await aux_voice_channels(groups)
    w = "A"
    dictionary = {}
    arr = []
    number_of_choices = an["question"]["number of choices"]
    final_word = chr(ord(w) + number_of_choices )
    while w < final_word:
        if an.get(w):
            online = []
            for user in an[w]:
                if user in online_students:
                    online.append(user)
            dictionary[w] = online
            arr.append((len(online), w))
        w=chr(ord(w) + 1)

    if an.get("sin respuesta") is not None:
        online = []
        for user in an["sin respuesta"]:
            if user in online_students:
                online.append(user)
        dictionary["sin respuesta"] = online
        arr.append((len(online), "sin respuesta"))
    arr.sort(reverse = True)
    for group in arr:
        users = dictionary[group[1]]
        for user in users:
            r = random.randint(0,len(voice_channels)-1)
            while(len(voice_channels[r].members)>maxsize):
                r = random.randint(0,len(voice_channels)-1)
            await user.move_to(voice_channels[r])


async def aux_voice_channels(groups):
    voice_channels = list()
    for group in groups:
        voice_channels.append(await get_voice_channel_in_group(group))
    return voice_channels

async def get_voice_channel_in_group(group):
    for voice in group.voice_channels:
        print(voice.name)
        pattern = re.compile(f"^A-voice-")
        if re.search(pattern, voice.name) is not None:
            return voice
    r = parse.search("A-voice-{:d}", group.name)
    voice_channel = await group.create_voice_channel(f"A-voice-{r[0]}")
    return voice_channel

async def aux_delete_groups(groups, i, f):
    for category in groups[i: f]:
        for channel in category.channels:
            print(f'Deleting a channels: ({channel.name})')
            await channel.delete()
        print(f'Deleting a category: {category.name}')
        await category.delete()


