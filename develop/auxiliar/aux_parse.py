
import discord
import asyncio



emojis = ['🔴', '🟠', '🟡', '🟢', '🔵', '🟣','🟤','⚫', '⚪', '🟥', '🟧', '🟨', '🟩', '🟦', '🟪', '🟫', '⬛', '⬜']

# revisa si la letra entregada en t_i corresponde a un letra de opcion
def is_choice(t_i, w, t_i_b, t_i_a):
    return t_i == w and t_i_b == '\n' and t_i_a == "."



# retorna un diccionario con la pregunta y las opciones
async def parse_choices(text):
    i_choices = []
    i = 1
    l = len(text) -1
    w = "A"
    # entrega los indices de las opciones
    while i < l and w <"Z":
        if is_choice( text[i], w, text[i-1], text[i+1]):
            i_choices += [i]
            w=chr(ord(w) + 1)
        i+=1
    if w == "A":
        print("la pregunta fue mal formulada")
        return None
    number_of_choices = ord(w) - ord("A")
    dictionary = {}
    w = "A"
    k_previous = 0
    choices = ''
    # guardamos en un dictionario la pregunta y las opciones
    e = 0
    for k in i_choices:
        if k_previous == 0:
            dictionary["question"] = text[k_previous : k-1]
            print("question is: " + dictionary["question"])
        else:
            dictionary[w] =  " [" + emojis[e] + "] " + text[k_previous : k-1]
            print("choice " + w + " is: "+ dictionary[w])
            choices += dictionary[w]
            w=chr(ord(w) + 1)
            e+=1
        k_previous = k
    dictionary[w] =  " [" + emojis[e] + "] " + text[k_previous :]
    print("choice " + w + " is: "+ dictionary[w])
    choices += dictionary[w]
    dictionary["choices"] = choices
    dictionary["number of choices"] = number_of_choices
    return dictionary
