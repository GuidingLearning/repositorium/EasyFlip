# global variables init
import dotenv, os

dotenv.load_dotenv()

TOKEN = os.getenv('DISCORD_TOKEN')
# TEST_GUILD_ID = os.getenv('DISCORD_TEST_GUILD_ID')
PROFESSOR_ROLE_NAME = os.getenv('PROFESSOR_ROLE_NAME')
AUXILIAR_ROLE_NAME = os.getenv('AUXILIAR_ROLE_NAME')
ASSISTANT_ROLE_NAME = os.getenv('ASSISTANT_ROLE_NAME')
#STUDENT_ROLE_NAME = os.getenv('STUDENT_ROLE_NAME')

