import discord
import asyncio

def create_embed_list(loq):
    embedVar = discord.Embed(title="Preguntas disponibles", color=discord.Color.blue())
    i=1
    for d in loq:
        embedVar.add_field(name= f"{i}) "+d["question"] + " ...", value = '\u200B', inline=False)
        i+=1
    embedVar.set_footer(text=f'''para hacer una pregunta es necesario utilizar el comando !ask X donde X es el numero de pregunta''')
    return embedVar


def create_embed_question(dictionary, time):
    embedVar = discord.Embed(title="Pregunta", description=dictionary["question"], color=discord.Color.green())
    w = "A"
    while dictionary.get(w) is not None:
        embedVar.add_field(name = dictionary[w], value = '\u200B', inline=False)
        w=chr(ord(w) + 1)
    embedVar.set_footer(text=f'''Tendrás {time} segundos para responder la pregunta. Solo contará tu primera reacción''')
    return embedVar



