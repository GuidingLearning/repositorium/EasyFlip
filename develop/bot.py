#!/usr/bin/env python
# -*- coding: utf-8 -*-
# bot.py

#from asyncio.windows_events import NULL
import sys
import discord
from discord import ChannelType
import asyncio
from discord.ext import commands
from discord.utils import get
import dotenv, os
import random
import re
from auxiliar.aux_parse import emojis,  parse_choices
from auxiliar.aux_question import create_embed_list, create_embed_question
from auxiliar.aux_groups import existing_groups, aux_create_groups, aux_students_online, aux_move_students, aux_move_general, aux_list_existing_groups, aux_delete_groups, aux_move_students_by_choices, aux_dispersed_groups
from auxiliar.global_variables import *

client = discord.Client()

dotenv.load_dotenv()

TOKEN = os.getenv('DISCORD_TOKEN')
PREFIX = "EF-"
prefix = "ef-"

intents = discord.Intents.all()
bot = commands.Bot(command_prefix='!', intents=intents)
questions = []
answers = {}

question_lock = asyncio.Lock()
answer_lock = asyncio.Lock()

groups_lock = asyncio.Lock()

GROUP_NAME_PATTERN = re.compile("^A-[A-Z]$")

################################################################################
################################    EVENTS      ################################
################################################################################

@bot.event
async def on_ready():
    print(f'{bot.user} is connected on ready')


################################################################################
###############################    COMMANDS      ###############################
################################################################################

###############################      START        ###############################

@bot.command(name='start', aliases=["initial-configuration"], help='initial configuration for the server.')
@commands.max_concurrency(number=1)
@commands.has_any_role(PROFESSOR_ROLE_NAME, AUXILIAR_ROLE_NAME, ASSISTANT_ROLE_NAME)
async def start_server(ctx):
    guild = ctx.guild
    categories = guild.categories
    category_texto = None
    category_voz = None
    for category in categories:
        if category.name == PREFIX+'Canales de texto':
            category_texto = category
        if category.name == PREFIX+'Canales de voz':
            category_voz = category
    if not category_texto:
        category_texto = await guild.create_category_channel(PREFIX+"Canales de texto")
    if not category_voz:
        category_voz = await guild.create_category_channel(PREFIX+"Canales de voz")
    voice_channel_list = category_voz.voice_channels
    exist_general_voice = False
    for voice_channel in voice_channel_list:
        if voice_channel.name == PREFIX+"General":
            exist_general_voice = True
            break
    if not exist_general_voice:
        await category_voz.create_voice_channel(PREFIX+"General")

    text_channel_list = category_texto.text_channels
    exist_general_text = False
    exist_command_text = False
    for text_channel in text_channel_list:
        if text_channel.name == prefix+"general":
            exist_general_text = True
        if text_channel.name == prefix+"commands":
            exist_command_text = True
    if not exist_general_text:
        await category_texto.create_text_channel(prefix+"general")
    
    if not exist_command_text:
        proffesor_role = get(guild.roles, name = PROFESSOR_ROLE_NAME)
        auxiliar_role = get(guild.roles, name = AUXILIAR_ROLE_NAME )
        assistan_role = get(guild.roles, name = ASSISTANT_ROLE_NAME)
        overwrites = {
            guild.default_role  : discord.PermissionOverwrite(read_messages=False, send_messages=False),
            proffesor_role : discord.PermissionOverwrite(read_messages=True, send_messages=True),
            auxiliar_role : discord.PermissionOverwrite(read_messages=True, send_messages=True),
            assistan_role : discord.PermissionOverwrite(read_messages=True, send_messages=False),
            guild.me : discord.PermissionOverwrite(read_messages = True, send_messages=True)
        }
        await category_texto.create_text_channel(prefix+"commands", overwrites=overwrites)
    
        

    

###############################    QUESTIONS      ###############################

@bot.command(name='create-question', aliases=["cq", "new-question"], help='create a new question.')
@commands.max_concurrency(number=1)
@commands.has_any_role(PROFESSOR_ROLE_NAME, AUXILIAR_ROLE_NAME, ASSISTANT_ROLE_NAME)
async def create_question(ctx):
    guild = ctx.guild
    commands_channel = get(guild.text_channels, name = prefix + "commands" )
    print(commands_channel)
    def check(msg):
        return msg.author == ctx.author and msg.channel == commands_channel

    await commands_channel.send("Escriba a continuación su pregunta!")
    try:
        question = await bot.wait_for("message", check = check, timeout=120)
    except asyncio.TimeoutError:
        await commands_channel.send("Te demoraste mucho en subir tu pregunta")
    print(question.content)

    await commands_channel.send("Cual es la alternativa correcta? Escriba la letra")
    try:
        correct_choice = await bot.wait_for("message", check = check, timeout=60)
    except asyncio.TimeoutError:
        await commands_channel.send("Te demoraste mucho en subir la alternativa correcta")
    cc = correct_choice.content.upper()
    print(cc)

    dictionary =await parse_choices(question.content)
    if dictionary is not None:
        if cc in dictionary:
            dictionary["correct_choice"] = cc
            async with question_lock:
                questions.append(dictionary)
            await commands_channel.send("Su pregunta ha sido registrada con exito!")
        else:
            await commands_channel.send("La alternativa correcta no existe dentro de las opciones")
    else:
        await commands_channel.send("No se han podido identificar alternativas")



@bot.command(name='list-of-questions', aliases=["list", "loq"], help='list of questions.')
@commands.max_concurrency(number=1)
@commands.has_any_role(PROFESSOR_ROLE_NAME, AUXILIAR_ROLE_NAME, ASSISTANT_ROLE_NAME)
async def list_of_questions(ctx):
    guild = ctx.guild
    commands_channel = get(guild.text_channels, name = prefix + "commands" )
    async with ctx.channel.typing():
        async with question_lock:
            loq = questions
        embed = create_embed_list(loq)
    await commands_channel.send(embed = embed)


async def wait_reaction(user, time, emoji_word):
    global answers
    def check2(reaction, user2):
            return user2 == user
    try:
        reaction, user2 = await bot.wait_for("reaction_add" , check = check2, timeout = time)
        async with answer_lock:
            print("el usuario " + user.name + " marcó" + reaction.emoji)
            if emoji_word.get(reaction.emoji):
                key = emoji_word[reaction.emoji]
                if answers.get(key):
                    answers[key] = answers[key] + [user]
                else:
                    answers[key] = [user]
            else: 
                print("fuera de la cantidad de emojis")
    except asyncio.TimeoutError:
        async with answer_lock:
            print("el usuario " + user.name + " no marcó")
            if answers.get("sin respuesta"):
                answers["sin respuesta"] = answers["sin respuesta"] + [user]
            else:
                answers["sin respuesta"] = [user]
        
        


@bot.command(name='ask-a-question', aliases=["ask", "ask-question"], help='ask a question.')
@commands.max_concurrency(number=1)
@commands.has_any_role(PROFESSOR_ROLE_NAME, AUXILIAR_ROLE_NAME, ASSISTANT_ROLE_NAME)
async def ask_a_question(ctx, i_question : int = None, time : int = 3):
    guild = ctx.guild
    general_channel = get(guild.text_channels, name = prefix + "general" )
    time *= 60
    #conseguimos la pregunta y cambiamos su formato
    async with question_lock:
        if i_question is None:
            i_question = len(questions) 
        dictionary = questions[i_question-1]

    async with answer_lock:
        global answers
        answers = {}
        answers["question"] = dictionary

    msg_question = await general_channel.send(embed= create_embed_question(dictionary, time))
    
    #se agregan los emojis como alternativas
    i=0 
    emoji_word = {}
    n = dictionary["number of choices"]
    while i < n:
        await msg_question.add_reaction(emojis[i])
        emoji_word[emojis[i]]=chr(ord("A") + i)
        i+=1

    online_students = aux_students_online(ctx.guild.members)
    tasks = list()
    for user in online_students:
        if user.voice and  user.voice.channel:
            print (user.name)     
            tasks.append(asyncio.create_task(wait_reaction(user, time, emoji_word)))
    for t in tasks:
        await t

    await general_channel.send("El tiempo de respuesta se ha acabado!")



###############################    GROUPS     ###############################


@bot.command(name='create-n-groups', aliases=["cg-n", "n-groups"], help='create N groups.')
@commands.max_concurrency(number=1)
@commands.has_any_role(PROFESSOR_ROLE_NAME, AUXILIAR_ROLE_NAME, ASSISTANT_ROLE_NAME)

async def create_n_groups(ctx, n : int):
    guild = ctx.guild
    general_channel = get(guild.voice_channels, name = "EF-General" )
    online_students = aux_students_online(ctx.guild.members)
    await aux_move_general(online_students, general_channel)
    online_students = aux_students_online(ctx.guild.members)
    maxsize = int(len(online_students) / n)
    maxsize += 1
    print("el tamaño maximo es " + str(maxsize))
    async with groups_lock:
        groups = existing_groups( ctx.guild )
        current_groups = aux_list_existing_groups( groups ) 
        if len(current_groups) > n:
            await  aux_delete_groups(current_groups, n, len(current_groups))
            current_groups = current_groups[:n]
        i = len(current_groups) + 1
        current_groups.extend(await aux_create_groups( ctx.guild, i, n))
    await aux_move_students(online_students, current_groups, maxsize)

    

@bot.command(name='create-groups-by-maxsize', aliases=["cg-m", "groups-maxsize"], help='create groups by maxsize.')
@commands.max_concurrency(number=1)
@commands.has_any_role(PROFESSOR_ROLE_NAME, AUXILIAR_ROLE_NAME, ASSISTANT_ROLE_NAME)

async def create_groups_by_maxsize(ctx, maxsize : int):
    guild = ctx.guild
    general_channel = get(guild.voice_channels, name = "EF-General" )
    online_students = aux_students_online(ctx.guild.members)
    await aux_move_general(online_students, general_channel)
    online_students = aux_students_online(ctx.guild.members)
    n = int(len(online_students) / maxsize)
    n +=1

    async with groups_lock:
        groups = existing_groups( ctx.guild )
        current_groups = aux_list_existing_groups( groups ) 
        if len(current_groups) > n:
            await aux_delete_groups(current_groups, n, len(current_groups))
            current_groups = current_groups[:n]
        i = len(current_groups) + 1
        current_groups.extend(await aux_create_groups( ctx.guild, i, n))
    await aux_move_students(online_students, current_groups, maxsize)

@bot.command(name='create-groups-by-choices', aliases=["cg-c", "groups-choice"], help='create groups by choices.')
@commands.max_concurrency(number=1)
@commands.has_any_role(PROFESSOR_ROLE_NAME, AUXILIAR_ROLE_NAME, ASSISTANT_ROLE_NAME)

async def create_groups_by_choice(ctx):
    guild = ctx.guild
    general_channel = get(guild.voice_channels, name = "EF-General" )
    online_students = aux_students_online(ctx.guild.members)
    await aux_move_general(online_students, general_channel)
    async with answer_lock:
        an = answers

    n = an["question"]["number of choices"] + 1

    online_students = aux_students_online(ctx.guild.members)

    async with groups_lock:
        groups = existing_groups( ctx.guild )
        current_groups = aux_list_existing_groups( groups ) 
        if len(current_groups) > n:
            await aux_delete_groups(current_groups, n, len(current_groups))
            current_groups = current_groups[:n]
        i = len(current_groups) + 1
        current_groups.extend(await aux_create_groups( ctx.guild, i, n))
    await aux_move_students_by_choices(online_students, current_groups, an)

@bot.command(name='create-groups-dispersed', aliases=["cg-d", "groups-d"], help='create groups dispersed.')
@commands.max_concurrency(number=1)
@commands.has_any_role(PROFESSOR_ROLE_NAME, AUXILIAR_ROLE_NAME, ASSISTANT_ROLE_NAME)

async def create_groups_by_dispersed(ctx, n : int):
    guild = ctx.guild
    general_channel = get(guild.voice_channels, name = "EF-General" )
    online_students = aux_students_online(ctx.guild.members)
    await aux_move_general(online_students, general_channel)
    async with answer_lock:
        an = answers

    online_students = aux_students_online(ctx.guild.members)
    maxsize = int(len(online_students) / n)
    maxsize += 1

    async with groups_lock:
        groups = existing_groups( ctx.guild )
        current_groups = aux_list_existing_groups( groups ) 
        if len(current_groups) > n:
            await aux_delete_groups(current_groups, n, len(current_groups))
            current_groups = current_groups[:n]
        i = len(current_groups) + 1
        current_groups.extend(await aux_create_groups( ctx.guild, i, n))
    await aux_dispersed_groups(online_students, current_groups, an, maxsize)


@bot.command(name='delete-A-groups', aliases=["dg-a", "delete-groups"], help='delete A-N groups.')
@commands.max_concurrency(number=1)
@commands.has_any_role(PROFESSOR_ROLE_NAME, AUXILIAR_ROLE_NAME, ASSISTANT_ROLE_NAME)

async def delete_A_groups(ctx):
    guild = ctx.guild
    general_channel = get(guild.voice_channels, name = "EF-General" )
    online_students = aux_students_online(ctx.guild.members)
    await aux_move_general(online_students, general_channel)
    groups = existing_groups( ctx.guild )
    current_groups = aux_list_existing_groups( groups ) 
    await aux_delete_groups(current_groups, 0, len(current_groups))

 
###############################    VOTING     ###############################
   
emojis_vote = ['😃','🙂','🤐','🙁','😖']
@bot.command(name='new-vote', aliases=["nv", "vote"], help='create a vote', pass_context=True)
@commands.max_concurrency(number=1)

async def new_vote(ctx, vote : str):
    for e in emojis_vote:
        await ctx.message.add_reaction(e)


###############################    MAIN     ###############################

def main(argv):
    # print(argv)
    bot.run(TOKEN)


if __name__ == '__main__':
    main(sys.argv[1:])

